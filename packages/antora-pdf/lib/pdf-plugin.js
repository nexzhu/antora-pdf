'use strict'

const { loadAsciiDoc, extractAsciiDocMetadata } = require('@djencks/asciidoc-loader')

const classifyPdfs = require('./classify/pdf-classifier')
const convertToPdf = require('./render/converter')
const pdfTemplate = require('./extensions/templates')
const bodyAttributesProcessor = require('./extensions/body-attributes-processor')
// const pdfIncludeProcessor = require('./extensions/include/pdf-include-processor')
const aincludeIncludeProcessor = require('./extensions/include/include-processor-wrapper')
const ainclude = require('@djencks/asciidoctor-ainclude')
const highlighter = require('asciidoctor-highlight.js')

module.exports.register = (eventEmitter, config = {}) => {
  console.log('antora-pdf-plugin registering', config)
  const suppressHtml = config.suppressHtml || false
  const pdfFiles = config.pdfFiles || []
  const defaultLayout = config.defaultLayout || 'pdf-default'
  const suspendServer = config.suspendServer
  var pdfPages = []
  var contentCatalog
  var uiCatalog
  const asciidocConfig = {}

  eventEmitter.on('afterClassifyContent', (playbook, cc) => {
    contentCatalog = cc
    classifyPdfs(pdfFiles, contentCatalog, pdfPages, suppressHtml)
  })

  eventEmitter.on('afterLoadUi', (playbook, uiCat) => { uiCatalog = uiCat })

  eventEmitter.on('beforeConvertDocuments', ({ contentCatalog, asciidocConfig: ac }) => {
    console.log('beforeConvertDocuments')
    asciidocConfig.attributes = Object.assign({}, ac.attributes)
    asciidocConfig.extensions = ac.extensions? ac.extensions.slice(0) : []
    asciidocConfig.converters = ac.converters? ac.converters.slice(0) : []

    asciidocConfig.extensions.push(ainclude)
    //We need a modified Antora include processor so that regular includes in aincluded pages have the correct
    //starting resource id for target resolution.
    asciidocConfig.extensions.push(aincludeIncludeProcessor)
    asciidocConfig.aincludeExtensions = [aincludeIncludeProcessor]
    asciidocConfig.extensions.push(bodyAttributesProcessor)
    asciidocConfig.extensions.push(highlighter)
    asciidocConfig.attributes['source-highlighter'] = 'highlightjs-ext'
    asciidocConfig.converters.push(pdfTemplate)

    //assign out property here?
    //separate step to convert pdfs. Use our own asciidoc loader..
    pdfPages = pdfPages.map((file) => {
      //TODO is there a better way to copy the file object?
      var pdfFile = Object.assign({},
        file,
        { src: Object.assign({}, file.src), contents: file.contents })
      delete pdfFile.out
      delete pdfFile.pub
      const removeHidden = pdfFile.src.basename[0] === '_' ? 1 : 0
      pdfFile.src.basename = pdfFile.src.basename.slice(removeHidden, -4) + 'pdf'
      if (removeHidden) {
        pdfFile.src.stem = pdfFile.src.stem.slice(1)
        pdfFile.src.relative = pdfFile.src.relative.slice(0, -pdfFile.src.stem.length - 6) + pdfFile.src.stem + '.adoc'
      }
      // pdfFile.src.mediaType = 'application/pdf'
      pdfFile.mediaType = 'text/html'
      pdfFile.src.family = 'attachment'
      // console.log('pdfFile.src', pdfFile.src)
      pdfFile = contentCatalog.addFile(pdfFile)
      if (pdfFile.out.basename.endsWith('.adoc')) {
        pdfFile.out.basename = pdfFile.out.basename.slice(0, -4) + 'pdf'
      }
      if (pdfFile.out.path.endsWith('.adoc')) {
        pdfFile.out.path = pdfFile.out.path.slice(0, -4) + 'pdf'
      }
      if (pdfFile.pub.url.endsWith('.adoc')) {
        pdfFile.pub.url = pdfFile.pub.url.slice(0, -4) + 'pdf'
      }
      // console.log('pdf file out at ', pdfFile.out)
      //TODO: This does not pre-extract the asciidoc attributes.
      const doc = loadAsciiDoc(pdfFile, contentCatalog, asciidocConfig)
      if (!pdfFile.asciidoc) {
        pdfFile.asciidoc = extractAsciiDocMetadata(doc)
        if (doc.hasAttribute('page-partial')) pdfFile.src.contents = pdfFile.contents
      }
      pdfFile.contents = Buffer.from(doc.convert())
      pdfFile.asciidoc.attributes['page-layout'] || (pdfFile.asciidoc.attributes['page-layout'] = defaultLayout)
      return pdfFile
    })
    console.log('beforeConvertDocuments exit')
  })

  eventEmitter.on('afterConvertDocuments', (playbook, pages) => {
    console.log('afterConvertDocuments')
    pdfPages.forEach((page) => pages.push(page))
  })

  eventEmitter.on('beforeMapSite', async (args) => {
    console.log('beforeMapSite')
    await convertToPdf(pdfPages, [contentCatalog, uiCatalog], suspendServer)
    console.log('beforeMapSite exit')
  })
}
