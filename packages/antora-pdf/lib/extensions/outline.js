'use strict'

const Opal = global.Opal
const ainclude = require('@djencks/asciidoctor-ainclude')
const $fileContext = ainclude.$fileContext
const DropAnchorRx = /<(?:a[^>+]+|\/a)>/g

//eslint-disable-next-line camelcase
const convert_outline = (node, opts = {}) => {
  //ruby code:
//   def convert_outline node, opts = {}
// return unless node.sections?
  const sections = []
  getSections(node, sections)
  if (!sections.length) {
    return
  }
  //   sectnumlevels = opts[:sectnumlevels] || (node.document.attributes['sectnumlevels'] || 3).to_i
  // toclevels = opts[:toclevels] || (node.document.attributes['toclevels'] || 2).to_i
  const sectnumlevels = Number.parseInt(opts.sectnumlevels || node.document.getAttribute('sectnumlevels', 3))
  const toclevels = Number.parseInt(opts.toclevels || node.document.getAttribute('toclevels', 2))
  // sections = node.sections
  // # FIXME top level is incorrect if a multipart book starts with a special section defined at level 0
  // result = [%(<ul class="sectlevel#{sections[0].level}">)]
  const result = [`<ul class="sectlevel${sections[0].level}">`]
  // sections.each do |section|
  sections.forEach((section) => {
    // slevel = section.level
    const slevel = section.level
    let stitle
    // if section.caption
    if (section.getCaption() && section.getCaption() !== Opal.nil) {
      //   stitle = section.captioned_title
      stitle = section.getCaptionedTitle()
      // elsif section.numbered && slevel <= sectnumlevels
    } else if (section.isNumbered() && section.isNumbered() !== Opal.nil && slevel <= sectnumlevels) {
      // if slevel < 2 && node.document.doctype == 'book'
      if (slevel < 2 && node.document.doctype === 'book') { //is this correct for nested docs?
        //   if section.sectname == 'chapter'
        if (section.sectname === 'chapter') {
          //eslint-disable-next-line max-len
          //     stitle =  %(#{(signifier = node.document.attributes['chapter-signifier']) ? "#{signifier} " : ''}#{section.sectnum} #{section.title})
          const signifier = node.document.attributes['chapter-signifier']
          stitle = `${signifier ? signifier + ' ' : ''}${section.$sectnum('.', '.')} ${section.getTitle()}`
          // elsif section.sectname == 'part'
        } else if (section.sectname === 'part') {
          //eslint-disable-next-line max-len
          // stitle =  %(#{(signifier = node.document.attributes['part-signifier']) ? "#{signifier} " : ''}#{section.sectnum nil, ':'} #{section.title})
          const signifier = node.document.attributes['part-signifier']
          stitle = `${signifier ? signifier + ' ' : ''}${section.$sectnum('.', ':')} ${section.getTitle()}`
          // else
        } else {
          // stitle = %(#{section.sectnum} #{section.title})
          stitle = `${section.$sectnum('.', '.')} ${section.getTitle()}`
          // end
        }
        // else
      } else {
        // stitle = %(#{section.sectnum} #{section.title})
        stitle = `${section.$sectnum('.', '.')} ${section.getTitle()}`
        // end
      }
      // else
    } else {
      // stitle = section.title
      stitle = section.getTitle()
      // end
    }
    // stitle = stitle.gsub DropAnchorRx, '' if stitle.include? '<a'
    if (stitle.includes('<a')) {
      stitle = stitle.replace(DropAnchorRx, '')
    }
    // result << %(<li><a href="##{section.id}">#{stitle}</a></li>)
    result.push(`<li class="toc-entry"><a href="#${section.id}">${stitle}</a></li>`)
    //eslint-disable-next-line max-len
    //   if slevel < toclevels && (child_toc_level = convert_outline section, toclevels: toclevels, sectnumlevels: sectnumlevels)
    //eslint-disable-next-line camelcase
    let child_toc_level
    //eslint-disable-next-line camelcase
    if (slevel < toclevels && (child_toc_level = convert_outline(section, { toclevels, sectnumlevels }))) {
      // result << child_toc_level
      result.push('<li class="toc-sublist">')
      result.push(child_toc_level)
      result.push('</li>')
    }
    // end
  })
  // result << '</ul>'
  result.push('</ul>')
  // result.join LF
  return result.join('\n')
// end
}

const getSections = (node, sections) => {
  node.blocks.forEach((block) => {
    if (block.context === 'section') {
      sections.push(block)
    } else if (block.context === 'document' && block[$fileContext]) {
      getSections(block, sections)
    }
  })
}

//eslint-disable-next-line camelcase
module.exports.convert_outline = convert_outline
