'use strict'

const computeRelativeUrlPath = require('../util/compute-relative-url-path')
const included = require('@djencks/asciidoctor-ainclude').included

const RESOURCE_ID_RX = /^(?:([^@:$]+)@)(?:(?:([^@:$]+):)(?:([^@:$]+))?:)(?:([^@:$]+)\$)([^:$][^@:$]*)$/
const RESOURCE_ID_RX_GROUP = { version: 1, component: 2, module: 3, family: 4, relative: 5 }
const FRAGMENT_SUFFIX = /^_[0-9]+$/

// var toLog = true

function key(src) {
  return `${src.version}@${src.component}:${src.module}:${src.family}$${src.relative}`;
}

function unkey(key) {
  const match = key.match(RESOURCE_ID_RX)
  if (!match) return
  return {
    version: match[RESOURCE_ID_RX_GROUP.version],
    component: match[RESOURCE_ID_RX_GROUP.component],
    module: match[RESOURCE_ID_RX_GROUP.module],
    family: match[RESOURCE_ID_RX_GROUP.family],
    relative: match[RESOURCE_ID_RX_GROUP.relative],
  }
}

var initialTargetInfo = {}

function initializeTopLevel (src) {
  // toLog = true
  const srcKey = key(src)
  initialTargetInfo = {}
  initialTargetInfo[srcKey] = { idprefix: '' }
  return srcKey
}

function findMatches(refCatalog, innerDoc, idprefix, fragment, targetPage) {
  var computed
  const matches = idprefix ?
    Object.entries(refCatalog).filter(([key, value]) => value.document === innerDoc &&
      (key === fragment || //unfortunately asciidoctor removes "same page" refid, so we can't distinguish between this page and top-page.
        key === (computed = idprefix + fragment) ||
        (key.startsWith(computed) && key.slice(computed.length).match(FRAGMENT_SUFFIX)))) :
    Object.entries(refCatalog).filter(([key, value]) => value.document === innerDoc &&
      (key === fragment || (key.startsWith(fragment) && key.slice(fragment.length).match(FRAGMENT_SUFFIX))))
  if (matches.length > 1) {
    console.warn(`multiple matches for fragment ${fragment} in page ${key(targetPage.src)}`)
  }
  // console.log(`\n findMatches: idprefix: ${idprefix}, fragment ${fragment}, matches: ${matches.join(', ')}`)
  return matches;
}

function getContent(content, value) {
  content = content || value.converted_title
  return content === Opal.nil? value.getAttribute('reftext') : content
}

/**
 * Converts the specified page reference to the data necessary to build an HTML link.
 *
 * Parses the page reference (page ID and optional fragment), resolves the corresponding file from
 * the content catalog, then grabs its publication (root-relative) path. If the relativize param is
 * true, transforms the root-relative path to a relative path from the current page to the target
 * page. Uses the resulting path to create the href for an HTML link that points to the published
 * target page.
 *
 * @memberof asciidoc-loader
 *
 * @param {String} refSpec - The target of an xref macro to a page, which is a page ID spec without
 * the .adoc extension and with an optional fragment identifier.
 * @param {String} content - The content (i.e., formatted text) of the link (undefined if not specified).
 * @param {File} currentPage - The virtual file for the current page.
 * @param {ContentCatalog} contentCatalog - The content catalog that contains the virtual files in the site.
 * @param {Boolean} [relativize=true] - Compute the target relative to the current page.
 * @returns {Object} A map ({ content, target, internal, unresolved }) containing the resolved
 * content and target to make an HTML link, and hints to indicate if the reference is either
 * internal or unresolved.
 */
/* There's a problem, in that for an xref to the top-level doc, Asciidoctor removes the path.
 * Therefore it's not possible to distinguish an included in-page reference to an include ref to the top level page.
 * We'll look first on the included page, then on the top level page.
 */
function convertPageRef (path, refSpec, content, currentSrc, contentCatalog, refCatalog, siteUrl, outerDoc) {
  let pageSpec
  let fragment
  let hash
  let targetPage
  if (~(hash = refSpec.indexOf('#'))) { // # present
    pageSpec = refSpec.substr(0, hash)
    hash = refSpec.substr(hash)
    fragment = hash.substr(1)
  } else if (path && path !== Opal.nil) { // path, no fragment
    pageSpec = refSpec
    hash = ''
  } else { //fragment, no path; might be current page or top page.
    pageSpec = currentSrc
    fragment = refSpec
    hash = '#' + fragment
  }
  try {
    if (!((targetPage = contentCatalog.resolvePage(pageSpec, unkey(currentSrc))) && targetPage.pub)) {
      // TODO log "Unresolved page ID"
      console.log(`unresolved page ID: path: ${path}, refSpec: ${refSpec}`)
      return { content: content || refSpec, target: '#' + refSpec, unresolved: true }
    }
  } catch (e) {
    // TODO log "Invalid page ID syntax" (or e.message)
    console.log(`invalid page ID: path: ${path}, refSpec: ${refSpec}`)
    return { content: content || refSpec, target: '#' + refSpec, unresolved: true }
  }
  const targetKey = key(targetPage.src)
  const targetInfo = included[targetKey] || initialTargetInfo[targetKey]

  // if (toLog) {
  //   console.log(`refCatalog keys:\n  ${Object.keys(refCatalog).join('\n  ')}`)
  //   console.log(`included keys:\n  ${Object.keys(included).join('\n  ')}`)
  //   console.log(`initialTargetInfo keys:\n  ${Object.keys(initialTargetInfo).join('\n  ')}`)
  // }
  // toLog = false
  // console.log(`path: ${path},\nrefSpec: ${refSpec},\npageSpec: ${pageSpec},\nkey: ${key(targetPage.src)}\n\n`)
  if (targetInfo) {
    const innerDoc = targetInfo.innerDoc
    const idprefix = targetInfo.idprefix
    if (fragment) {
      var matches = findMatches(refCatalog, innerDoc, idprefix, fragment, targetPage);
      if (matches.length > 0) {
        const [key, value] = matches[0]
        fragment = key
        content = getContent(content, value)
      }  else {
        matches = findMatches(refCatalog, outerDoc, idprefix, fragment, targetPage)
        if (matches.length > 0) {
          const [key, value] = matches[0]
          fragment = key
          content = getContent(content, value)
        }
      }
    } else {
      fragment = targetInfo.docid
      content = getContent(content, innerDoc.blocks[0])
    }
  }
  const target = targetInfo ?
    fragment ? `#${fragment}` : `#${targetInfo.docid}` :
    pageSpec? siteUrl + targetPage.pub.url + hash : hash
  if (!content) {
    if (hash) {
      content = pageSpec + hash
    } else {
      content =
        (currentSrc.family === 'nav'
          ? (targetPage.asciidoc || {}).navtitle
          : (targetPage.asciidoc || {}).xreftext) || pageSpec
    }
  }
  return { content: content || fragment || target, target, internal: targetInfo || !pageSpec }
}

function convertImageRef (resourceSpec, currentPage, topPage, contentCatalog) {
  const image = contentCatalog.resolveResource(resourceSpec, unkey(currentPage), 'image', ['image'])
  if (image) return computeRelativeUrlPath(topPage.pub.url, image.pub.url)
}

module.exports = { convertPageRef, convertImageRef, initializeTopLevel }
